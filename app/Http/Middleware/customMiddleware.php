<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class customMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $requestIp = $request->ip();
        if ($requestIp !== "103.178.60.248" || $requestIp !== "127.0.0.1") {
            return \response(array("status" => false, "message" => "Your IP is not allowed"));
        }
        return $next($request);
    }
}
