<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Subjects extends Model
{
    use HasFactory;

    /**
     * creating a relationship between subject & results.
     *
     * @return HasOne
     */
    public function result(): HasOne
    {
        return $this->hasOne(Results::class, 'subject_id', 'id');
    }
}
