<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Results extends Model
{
    use HasFactory;

    /**
     * creates relationship between results & subjects.
     *
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(Subjects::class, 'subject_id');
    }

    /**
     * relationship between result and students
     * @return HasOne
     */
    public function student(): HasOne
    {
        return $this->hasOne(Student::class, 'roll', 'roll');
    }
}
