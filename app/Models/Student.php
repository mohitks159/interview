<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Student extends Model
{
    use HasFactory;

    /**
     * Relationship between Student & their Results.
     *
     * @return HasMany
     */
    public function results(): HasMany
    {
        return $this->hasMany(Results::class, 'roll', 'roll');
    }
}
